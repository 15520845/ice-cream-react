import React from 'react';
import './App.css';
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import LoginPage from "./Component/Customer/Login"
import HomePage from "./Component/Customer/HomePage"
import Register from "./Component/Customer/Register"
import Profile from "./Component/Customer/Profile"
import ChangePassword from './Component/Customer/ChangePassword'
import YourRecipe from './Component/Customer/Recipe/YourRecipe'
import Faq from './Component/Customer/Faq/Faq'
import FeedBack from './Component/Customer/Feedback/Feedback'
import Order from './Component/Customer/Order/Order'
import RecipeDetail from './Component/Customer/Recipe/RecipeDetail'
import UpdateProfile from './Component/Customer/UpdateProfile'
import AdminLogin from './Component/Admin/Login'
import ListCustomer from './Component/Admin/Customer/ListCustomer'
import UpdateCustomer from './Component/Admin/Customer/UpdateCustomer'
import ListRecipe from './Component/Admin/Recipe/ListRecipe';
import EditRecipe from './Component/Admin/Recipe/EditRecipe';
import PreviewRecipe from './Component/Admin/Recipe/PreviewRecipe'
import AddNewRecipe from './Component/Admin/Recipe/AddNewRecipe'
import PreviewAddRecipe from './Component/Admin/Recipe/PreviewAddRecipe'
import ListUserRecipe from './Component/Admin/Recipe/ListUserRecipe'
import ListFAQ from './Component/Admin/FAQ/listFAQ'
import FAQFeedback from './Component/Admin/FAQ/FAQFeedback'
import AddNewFAQ from './Component/Admin/FAQ/AddNewFAQ'
import ListFeedBack from './Component/Admin/FeedBack/ListFeedBack'
function App() {
  return (
    <div className="App">
      <Router>
        
        <Switch>
          <Route exact path={["/", "/home"]} component={HomePage} />
          <Route exact path="/login" component={LoginPage} />
          <Route exact path="/register" component={Register} />
          <Route path="/profile" component={Profile} />
          <Route path="/yourRecipe" component={YourRecipe} />
          <Route path="/changePassword" component={ChangePassword} />
          <Route path="/faq" component={Faq} />
          <Route path="/feedBack" component={FeedBack} />
          <Route path="/order" component={Order} />
          <Route path="/recipedetail" component={RecipeDetail} />
          <Route path="/updateProfile" component={UpdateProfile} />
          <Route path="/admin" component={AdminLogin} />
          <Route  path="/listCustomer" component={ListCustomer} />
          <Route  path="/updateCustomer" component={UpdateCustomer} />
          <Route  path="/listRecipe" component={ListRecipe} />
          <Route  path="/editRecipe" component={EditRecipe} />
          <Route  path="/previewRecipe" component={PreviewRecipe} />
          <Route  path="/addNewRecipe" component={AddNewRecipe} />
          <Route  path="/previewAddRecipe" component={PreviewAddRecipe} />
          <Route  path="/userRecipe" component={ListUserRecipe} />
          <Route  path="/listFaq" component={ListFAQ} />
          <Route  path="/FAQFeedback" component={FAQFeedback} />
          <Route  path="/addNewFAQ" component={AddNewFAQ} />
          <Route  path="/listFeedback" component={ListFeedBack} />
        </Switch>
      </Router>

    </div>
  );
}

export default App;
