import React, { Component } from 'react'
import Headers from "../Layout/Header"
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom"
import { Button } from 'react-bootstrap';
import RecipeService from '../Service/RecipeService'

export default class ListRecipe extends Component {
    constructor(props) {
        super(props);

        this.state = {
            content: []
        };
    }

    componentDidMount() {
        RecipeService.getListRecipe().then(
            response => {
                this.setState({
                    content: response.data
                });
            },
            error => {
                this.setState({
                    content:
                        (error.response && error.response.data) ||
                        error.message ||
                        error.toString()
                });
            }
        );
        console.log(this.state.content)
    }

    render() {
        return (
            <div className="container-fluid">
                <Headers></Headers>
                <div className="row content">
                    <div className="col-sm-2"></div>
                    <div className="col-sm-8">
                        <div className="form-group row">
                            <div className="col-sm-4"><Link to={"/addNewRecipe"} > Add New Recipe </Link></div>
                            <div className="col-sm-4"></div>
                            <div className="col-sm-4">
                                <div className="col-sm-6">
                                    <Link to={"/userRecipe"} > User Recipe </Link>
                                </div>
                            </div>
                            {this.state.content.map(d =>
                                <table className="table table-hover">
                                    <tbody>
                                        <tr>
                                            <td >{d.recipeId}</td>
                                            <td >{d.name}</td>
                                            <td><div className="image-container" ><img src={"image/" + d.image} alt="icon" width="200" /> </div></td>
                                            <td >{d.author}</td>
                                            <td>{d.viewNumber}</td>
                                            <td>{d.uploadDate}</td>
                                            <td><input type="checkbox" readOnly
                                                checked={d.enableStatus} />
                                            </td>
                                            <td>
                                                <Link to={{
                                                    pathname: "/editRecipe",
                                                    state: {
                                                        recipeEditId: d.recipeId
                                                    }
                                                }}>
                                                    Edit
                                                </Link>
                                            </td>
                                            <td>
                                                <Link to={{
                                                    pathname: "/previewRecipe",
                                                    state: {
                                                        recipePreviewId: d.recipeId
                                                    }
                                                }}>
                                                    Preview
                                                </Link>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            )}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
