import React, { Component } from 'react'
import Header from "../Layout/Header"
import RecipeService from "../Service/RecipeService"
import { Media } from 'react-bootstrap'
import { Link } from "react-router-dom"

export default class PreviewRecipe extends Component {

    constructor(props) {
        super(props);

        this.state = {
            content: [],
        };
    }

    componentDidMount() {
        const { recipePreviewId } = this.props.location.state;
        RecipeService.getRecipeDetail(recipePreviewId).then(
            response => {
                this.setState({
                    content: [response.data]
                });
            },
            error => {
                this.setState({
                    content:
                        (error.response && error.response.data) ||
                        error.message ||
                        error.toString()
                });
            }
        );
        console.log(this.state.content)
    }


    render() {
        return (
            <div className="container-fluid">
                <Header></Header>
                <div className="row content">
                    <div className="col-sm-2"></div>
                    {this.state.content.map(d =>
                    
                    <div className="col-sm-8">
                        <div className="form-group row">
                            <div className="col-sm-4">Preview Recipe</div>
                            <div className="col-sm-4"></div>
                            <div className="col-sm-4"></div>
                        </div>
                        {console.log(this.state.content)}

                            <Media>
                                <div>
                                    <div className=" row">
                                        <img
                                            width={200}
                                            height={200}
                                            className="mr-3"
                                            src={"/image/" + d.image}
                                            alt={d.viewNumber}
                                        />
                                    </div>
                                    <div className=" row">
                                        {d.viewNumber}
                                    </div>

                                </div>

                                <Media.Body>
                                    <p>
                                        {d.description}
                                    </p>
                                    <br></br>
                                    <hr></hr>
                                    <p>{d.details}</p>
                                    <hr></hr>
                                </Media.Body>
                            </Media>
                        
                        <div className="form-group row">
                            <div className="col-md-2"></div>
                            <div className="col-md-8">
                                <div className="form-group row">
                                    <div className="col-md-6">
                                        <Link to={"/listRecipe"}>
                                            <button >Finish</button>
                                        </Link>
                                    </div>
                                    <div className="col-md-6">
                                        <Link to={{
                                            pathname: "/editRecipe",
                                            state: {
                                                recipeEditId: d.recipeId
                                            }
                                        }}> <button>Edit</button>
                                        </Link>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-2"></div>
                        </div>
                    </div>
                    )}
                </div>
            </div>
        )
    }
}
