import React, { Component } from 'react'
import Header from "../Layout/Header"
import RecipeService from "../Service/RecipeService"
import { Media } from 'react-bootstrap'
import { Link } from "react-router-dom"
import Form from "react-validation/build/form";
import CheckButton from "react-validation/build/button";

export default class PreviewAddRecipe extends Component {

    constructor(props) {
        super(props);
        this.handlePostRecipe = this.handlePostRecipe.bind(this);

        this.state = {
            content: [],
            message: "",
            successful: false
        };
    }

    componentDidMount() {
        const { contentRecipe } = this.props.location.state;
        this.setState({
            content: contentRecipe
        })
        console.log(this.props.location.state)
    }

    handlePostRecipe(e) {
        e.preventDefault();

        this.setState({
            message: "",
            successful: true
        });
        console.log(this.state.content)
        this.state.content.map(d => {
            if (this.checkBtn.context._errors.length === 0) {
                RecipeService.postRecipe(d.name, d.image, d.description, d.details, d.author, d.uploadDate, d.enableStatus).then(

                    response => {
                        this.setState({
                            message: response.data.message,
                            successful: true
                        });
                    },
                    error => {
                        const resMessage =
                            (error.response &&
                                error.response.data &&
                                error.response.data.message) ||
                            error.message ||
                            error.toString();

                        this.setState({
                            successful: false,
                            message: resMessage
                        });
                    }
                );
            } else {
                this.setState({
                    successful: false
                });
            }
        })

    }

    render() {
        return (
            <div className="container-fluid">
                <Header></Header>
                <Form
                    onSubmit={this.handlePostRecipe}
                    ref={c => {
                        this.form = c;
                    }}>
                    <div className="row content">
                        <div className="col-sm-2"></div>
                        {this.state.content.map(d =>
                            <div className="col-sm-8">
                                <div className="form-group row">
                                    <div className="col-sm-4">Preview New Recipe</div>
                                    <div className="col-sm-4"></div>
                                    <div className="col-sm-4"></div>
                                </div>

                                <Media>
                                    <div>
                                        <div className=" row">
                                            <img
                                                width={200}
                                                height={200}
                                                className="mr-3"
                                                src={"/image/" + d.image}
                                            />
                                        </div>
                                        <div className=" row">
                                            {d.viewNumber}
                                        </div>

                                    </div>

                                    <Media.Body>
                                        <p>
                                            {d.description}
                                        </p>
                                        <br></br>
                                        <hr></hr>
                                        <p>{d.details}</p>
                                        <hr></hr>
                                    </Media.Body>
                                </Media>

                                <div className="form-group row">
                                    <div className="col-md-2"></div>
                                    <div className="col-md-8">
                                        <div className="form-group row">
                                            <div className="col-md-6">
                                                <button >Finish</button>
                                            </div>
                                            <div className="col-md-6">
                                                <Link to={{
                                                    pathname: "/addNewRecipe",
                                                    state: {
                                                        recipeEditId: d.recipeId
                                                    }
                                                }}> <button>Edit</button>
                                                </Link>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-2"></div>
                                </div>

                            </div>
                        )}
                        {this.state.message && (
                            <div className="form-group">
                                <div
                                    className={
                                        this.state.successful
                                            ? "alert alert-success"
                                            : "alert alert-danger"
                                    }
                                    role="alert"
                                >
                                    {this.state.message}
                                </div>
                            </div>
                        )}
                        <CheckButton
                            style={{ display: "none" }}
                            ref={c => {
                                this.checkBtn = c;
                            }}
                        />
                    </div>
                </Form>
            </div>
        )
    }
}
