import React, { Component } from 'react'
import "react-datepicker/dist/react-datepicker.css"
import { Link } from "react-router-dom"
import CheckButton from "react-validation/build/button"
import Form from "react-validation/build/form"
import Input from "react-validation/build/input"
import Header from "../Layout/Header"
import RecipeService from "../Service/RecipeService"
import Textarea from "react-validation/build/textarea";

const required = value => {
    if (!value) {
        return (
            <div className="alert alert-danger" role="alert">
                This field is required!
            </div>
        );
    }
};
export default class EditRecipe extends Component {
    constructor(props) {
        super(props);
        this.onChangeName = this.onChangeName.bind(this);
        this.onChangeDescription = this.onChangeDescription.bind(this);
        this.onChangeDetails = this.onChangeDetails.bind(this);
        this.fileChangedHandler = this.fileChangedHandler.bind(this);
        this.onChangeAuthor = this.onChangeAuthor.bind(this);
        this.onChangeStatus = this.onChangeStatus.bind(this);
        this.handleEditRecipe = this.handleEditRecipe.bind(this);

        this.state = {
            recipeId: "",
            name: "",
            image: "",
            description: "",
            details: "",
            author: "",
            viewNumber: "",
            uploadDate: "",
            enableStatus: "",
            successful: false,
            message: ""
        };
    }

    onChangeName(e) {
        this.setState({
            name: e.target.value
        });
    }

    onChangeDescription(e) {
        this.setState({
            description: e.target.value
        });
    }

    onChangeDetails(e) {
        this.setState({
            details: e.target.value
        });
    }

    onChangeAuthor(e) {
        this.setState({
            author: e.target.value
        });
    }

    onChangeStatus(e) {
        this.setState({
            enableStatus: e.target.value
        });
    }

    fileChangedHandler(e) {
        this.setState({
            image: "image/" + e.target.files[0].name
        })
        let reader = new FileReader();

        reader.onloadend = () => {
            this.setState({
                imagePreviewUrl: reader.result
            });
        }
        reader.readAsDataURL(e.target.files[0])

    }
    componentDidMount() {
        const { recipeEditId } = this.props.location.state;
        RecipeService.getRecipeDetail(recipeEditId).then(
            response => {
                this.setState({
                    recipeId: response.data.recipeId,
                    name: response.data.name,
                    image: response.data.image,
                    description: response.data.description,
                    details: response.data.details,
                    author: response.data.author,
                    viewNumber: response.data.viewNumber,
                    uploadDate: response.data.uploadDate,
                    enableStatus: response.data.enableStatus,
                });
            },
            error => {
                this.setState({
                    content:
                        (error.response && error.response.data) ||
                        error.message ||
                        error.toString()
                });
            }
        );
    }

    handleEditRecipe(e) {
        e.preventDefault();

        this.setState({
            message: "",
            successful: false
        });

        this.form.validateAll();

        if (this.checkBtn.context._errors.length === 0) {
            RecipeService.editRecipe(
                this.state.recipeId,
                this.state.name,
                this.state.image,
                this.state.description,
                this.state.details,
                this.state.author,
                this.state.enableStatus
            ).then(
                response => {
                    this.setState({
                        message: response.data.message,
                        successful: true
                    });
                },
                error => {
                    const resMessage =
                        (error.response &&
                            error.response.data &&
                            error.response.data.message) ||
                        error.message ||
                        error.toString();

                    this.setState({
                        successful: false,
                        message: resMessage
                    });
                }
            );
        }
    }

    render() {
        let $imagePreview = (<div className="previewText image-container"></div>);
        if (this.state.image) {
            $imagePreview = (<div className="image-container" ><img src={"image/" + this.state.image} alt="icon" width="200" /> </div>);
        }
        return (
            <div className="container-fluid">
                <Header></Header>
                <div className="row content">
                    <div className="col-sm-2"></div>
                    <div className="col-sm-8">
                        <div className="form-group row">
                            <div className="col-sm-4">Edit Recipe</div>
                            <div className="col-sm-4"></div>
                            <div className="col-sm-4"></div>
                        </div>
                        <Form
                            onSubmit={this.handleEditRecipe}
                            ref={c => {
                                this.form = c;
                            }}
                        >
                            {!this.state.successful && (
                                <div>
                                    <div className="form-group row">
                                        <label className="col-md-4 col-form-label" htmlFor="recipeId">RecipeID</label>
                                        <div className="col-sm-8">
                                            <Input
                                                type="text"
                                                className="form-control"
                                                name="recipeId"
                                                readOnly
                                                value={this.state.recipeId}
                                                validations={[required]}
                                            />
                                        </div>
                                    </div>

                                    <div className="form-group row">
                                        <label className="col-md-4 col-form-label" htmlFor="name">Name</label>
                                        <div className="col-sm-8">
                                            <Input
                                                type="text"
                                                className="form-control"
                                                name="name"
                                                value={this.state.name}
                                                onChange={this.onChangeName}
                                                validations={[required]}
                                            />
                                        </div>
                                    </div>
                                    <div className="form-group row">
                                        <label className="col-md-4 col-form-label" htmlFor="avatar">Avatar</label>
                                        <div className="col-sm-8">
                                            <input type="file" name="avatar" onChange={this.fileChangedHandler} />
                                        </div>
                                    </div>
                                    <div className="form-group row">
                                        <label className="col-md-4 col-form-label" ></label>
                                        <div className="col-sm-8"> {$imagePreview}</div>
                                    </div>

                                    <div className="form-group row">
                                        <label className="col-sm-4 col-form-label" htmlFor="description">Description: </label>
                                        <div className="col-sm-8">
                                            <Textarea
                                                className="form-control"
                                                name="description"
                                                value={this.state.description}
                                                onChange={this.onChangeDescription}
                                                validations={[required]}
                                            />
                                        </div>
                                    </div>

                                    <div className="form-group row">
                                        <label className="col-sm-4 col-form-label" htmlFor="details">Details: </label>
                                        <div className="col-sm-8">
                                            <Textarea
                                                className="form-control"
                                                name="details"
                                                value={this.state.details}
                                                onChange={this.onChangeDetails}
                                                validations={[required]}
                                            />
                                        </div>
                                    </div>

                                    <div className="form-group row">
                                        <label className="col-md-4 col-form-label" htmlFor="author">Author:</label>
                                        <div className="col-sm-8">
                                            <Input
                                                type="text"
                                                className="form-control"
                                                name="author"
                                                value={this.state.author}
                                                onChange={this.onChangeAuthor}
                                                validations={[required]}
                                            />
                                        </div>
                                    </div>

                                    <div className="form-group row">
                                        <label className="col-md-4 col-form-label" htmlFor="viewNumber">View Number:</label>
                                        <div className="col-sm-8">
                                            <Input
                                                type="text"
                                                readOnly
                                                className="form-control"
                                                name="viewNumber"
                                                value={this.state.viewNumber}
                                                validations={[required]}
                                            />
                                        </div>
                                    </div>

                                    <div className="form-group row">
                                        <label className="col-md-4 col-form-label" htmlFor="uploadDate">Upload Date:</label>
                                        <div className="col-sm-8">
                                            <Input
                                                type="text"
                                                readOnly
                                                className="form-control"
                                                name="uploadDate"
                                                value={this.state.uploadDate}
                                                validations={[required]}
                                            />
                                        </div>
                                    </div>

                                    <div className="form-group row">
                                        <label className="col-md-4 col-form-label" htmlFor="enableStatus">Enable Status:</label>
                                        <div className="col-sm-8">
                                            <Input
                                                type="checkbox"
                                                className="form-control"
                                                name="enableStatus"
                                                checked={this.state.enableStatus}
                                                onChange={this.onChangeStatus}
                                            />
                                        </div>
                                    </div>

                                    <div className="form-group row">
                                        <div className="col-md-2"></div>
                                        <div className="col-md-8">
                                            <div className="form-group row">
                                                <div className="col-md-6">
                                                    <button className="btn btn-primary btn-block">Edit</button>
                                                </div>
                                                <div className="col-md-6">
                                                   <Link to={"/listRecipe"}><button>Cancel</button></Link> 
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-md-2"></div>
                                    </div>
                                </div>
                            )}

                            {this.state.message && (
                                <div className="form-group">
                                    <div
                                        className={
                                            this.state.successful
                                                ? "alert alert-success"
                                                : "alert alert-danger"
                                        }
                                        role="alert"
                                    >
                                        {this.state.message}
                                    </div>
                                </div>
                            )}
                            <CheckButton
                                style={{ display: "none" }}
                                ref={c => {
                                    this.checkBtn = c;
                                }}
                            />
                        </Form>
                    </div>
                    <div className="col-sm-2"></div>
                </div>
            </div>
        )
    }
}
