import React, { Component } from 'react'
import "react-datepicker/dist/react-datepicker.css"
import { Link } from "react-router-dom"
import CheckButton from "react-validation/build/button"
import Form from "react-validation/build/form"
import Input from "react-validation/build/input"
import Header from "../Layout/Header"
import RecipeService from "../Service/RecipeService"
import Textarea from "react-validation/build/textarea";

const required = value => {
    if (!value) {
        return (
            <div className="alert alert-danger" role="alert">
                This field is required!
            </div>
        );
    }
};
export default class AddNewRecipe extends Component {
    constructor(props) {
        super(props);
        this.onChangeName = this.onChangeName.bind(this);
        this.onChangeDescription = this.onChangeDescription.bind(this);
        this.onChangeDetails = this.onChangeDetails.bind(this);
        this.fileChangedHandler = this.fileChangedHandler.bind(this);
        this.onChangeAuthor = this.onChangeAuthor.bind(this);
        this.onChangeStatus = this.onChangeStatus.bind(this);
        this.onChangeUploadDate = this.onChangeUploadDate.bind(this);

        this.state = {
            recipeId: "",
            name: "",
            image: "",
            description: "",
            details: "",
            author: "",
            uploadDate: "",
            enableStatus: false,
            successful: false,
            message: ""
        };
    }

    onChangeName(e) {
        this.setState({
            name: e.target.value
        });
    }

    onChangeDescription(e) {
        this.setState({
            description: e.target.value
        });
    }

    onChangeDetails(e) {
        this.setState({
            details: e.target.value
        });
    }

    onChangeAuthor(e) {
        this.setState({
            author: e.target.value
        });
    }

    onChangeStatus(e) {
        this.setState({
            enableStatus: e.target.checked
        });
    }

    onChangeUploadDate(e) {
        this.setState({
            uploadDate: e.target.value
        });
    }

    fileChangedHandler(e) {
        this.setState({
            image: "images/" + e.target.files[0].name
        })
        let reader = new FileReader();

        reader.onloadend = () => {
            this.setState({
                imagePreviewUrl: reader.result
            });
        }
        reader.readAsDataURL(e.target.files[0])

    }

    render() {
        let $imagePreview = (<div className="previewText image-container"></div>);
        if (this.state.image) {
            $imagePreview = (<div className="image-container" ><img src={this.state.imagePreviewUrl} alt="icon" width="200" /> </div>);
        }
        return (
            <div className="container-fluid">
                <Header></Header>
                <div className="row content">
                    <div className="col-sm-2"></div>
                    <div className="col-sm-8">
                        <div className="form-group row">
                            <div className="col-sm-4">Add New Recipe</div>
                            <div className="col-sm-4"></div>
                            <div className="col-sm-4"></div>
                        </div>
                        <Form
                            onSubmit={this.handleAddNewRecipe}
                            ref={c => {
                                this.form = c;
                            }}
                        >
                            <div>
                                <div className="form-group row">
                                    <label className="col-md-4 col-form-label" htmlFor="name">Name</label>
                                    <div className="col-sm-8">
                                        <Input
                                            type="text"
                                            className="form-control"
                                            name="name"
                                            value={this.state.name}
                                            onChange={this.onChangeName}
                                            validations={[required]}
                                        />
                                    </div>
                                </div>
                                <div className="form-group row">
                                    <label className="col-md-4 col-form-label" htmlFor="avatar">Avatar</label>
                                    <div className="col-sm-8">
                                        <input type="file" name="avatar" onChange={this.fileChangedHandler} />
                                    </div>
                                </div>
                                <div className="form-group row">
                                    <label className="col-md-4 col-form-label" ></label>
                                    <div className="col-sm-8"> {$imagePreview}</div>
                                </div>

                                <div className="form-group row">
                                    <label className="col-sm-4 col-form-label" htmlFor="description">Description: </label>
                                    <div className="col-sm-8">
                                        <Textarea
                                            className="form-control"
                                            name="description"
                                            value={this.state.description}
                                            onChange={this.onChangeDescription}
                                            validations={[required]}
                                        />
                                    </div>
                                </div>

                                <div className="form-group row">
                                    <label className="col-sm-4 col-form-label" htmlFor="details">Details: </label>
                                    <div className="col-sm-8">
                                        <Textarea
                                            className="form-control"
                                            name="details"
                                            value={this.state.details}
                                            onChange={this.onChangeDetails}
                                            validations={[required]}
                                        />
                                    </div>
                                </div>

                                <div className="form-group row">
                                    <label className="col-md-4 col-form-label" htmlFor="author">Author:</label>
                                    <div className="col-sm-8">
                                        <Input
                                            type="text"
                                            className="form-control"
                                            name="author"
                                            value={this.state.author}
                                            onChange={this.onChangeAuthor}
                                            validations={[required]}
                                        />
                                    </div>
                                </div>

                                <div className="form-group row">
                                    <label className="col-md-4 col-form-label" htmlFor="uploadDate">Upload Date:</label>
                                    <div className="col-sm-8">
                                        <Input
                                            type="text"
                                            className="form-control"
                                            name="uploadDate"
                                            value={this.state.uploadDate}
                                            validations={[required]}
                                            onChange={this.onChangeUploadDate}
                                        />
                                    </div>
                                </div>

                                <div className="form-group row">
                                    <label className="col-md-4 col-form-label" htmlFor="enableStatus">Enable Status:</label>
                                    <div className="col-sm-8">
                                        <Input
                                            type="checkbox"
                                            className="form-control"
                                            name="enableStatus"
                                            checked={this.state.enableStatus}
                                            onChange={this.onChangeStatus}
                                        />
                                    </div>
                                </div>

                                <div className="form-group row">
                                    <div className="col-md-2"></div>
                                    <div className="col-md-8">
                                        <div className="form-group row">
                                            <div className="col-md-6">
                                                <Link to={{
                                                    pathname: "/previewAddRecipe",
                                                    state: {
                                                        contentRecipe: [{ "name": this.state.name, "image": this.state.image, "description": this.state.description, "details": this.state.details, "author": this.state.author, "uploadDate": this.state.uploadDate, "enableStatus": this.state.enableStatus }]
                                                    }
                                                }}>
                                                    <button>Preview</button>
                                                </Link>
                                            </div>
                                            <div className="col-md-6">
                                                <Link to={"/listRecipe"}><button>Cancel</button></Link>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-2"></div>
                                </div>
                            </div>
                        </Form>
                    </div>
                    <div className="col-sm-2"></div>
                </div>
            </div>
        )
    }
}
