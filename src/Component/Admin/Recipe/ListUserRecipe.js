import React, { Component } from 'react'
import Headers from "../Layout/Header"
import { Link } from "react-router-dom"
import RecipeService from '../Service/RecipeService'

export default class ListUserRecipe extends Component {
    constructor(props) {
        super(props);

        this.state = {
            content: []
        };
    }

    componentDidMount() {
        RecipeService.getListUserRecipe().then(
            response => {
                this.setState({
                    content: response.data
                });
            },
            error => {
                this.setState({
                    content:
                        (error.response && error.response.data) ||
                        error.message ||
                        error.toString()
                });
            }
        );
        console.log(this.state.content)
    }
    delete(id) {
        RecipeService.deleteUserRecipe(id).then(
            response => {
                this.setState({
                    message: "successful",
                    successful: true
                   
                });
                window.location.reload();
            },
            error => {
                const resMessage =
                    (error.response &&
                        error.response.data &&
                        error.response.data.message) ||
                    error.message ||
                    error.toString();

                this.setState({
                    successful: false,
                    message: resMessage
                });
            }
        )
    }

    render() {
        return (
            <div className="container-fluid">
                <Headers></Headers>
                <div className="row content">
                    <div className="col-sm-2"></div>
                    <div className="col-sm-8">
                        <div className="form-group row">
                            <div className="col-sm-4"></div>
                            <div className="col-sm-4">User Recipe</div>
                            <div className="col-sm-4"></div>
                            {this.state.content.map(d =>
                                <table className="table table-hover">
                                    <tbody>
                                        <tr>
                                            <td >{d.id}</td>
                                            <td >{d.name}</td>
                                            <td><div className="image-container"><img src={"image/"+d.image} alt="icon" width="200" /> </div></td>
                                            <td >{d.description}</td>
                                            <td>{d.details}</td>
                                            <td>{d.customer.id}</td>
                                            <td><input type="checkbox" readOnly
                                                checked={d.enableStatus} />
                                            </td>
                                            <td>
                                                <Link to={"/listUserRecipe"} onClick={()=>this.delete(d.id)}>
                                                    Delete
                                                </Link>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            )}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
