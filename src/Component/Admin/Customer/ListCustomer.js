import React, { Component } from 'react'
import { Button } from 'react-bootstrap'
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table'
import { Link } from "react-router-dom"
import Headers from "../Layout/Header"
import CustomerService from "../Service/CustomerService"


export default class ListCustomer extends Component {

    constructor(props) {
        super(props);

        this.state = {
            customerList: []
        };
    }

    componentDidMount() {

        CustomerService.getListCustomer().then(
            response => {
                this.setState({
                    customerList: response.data
                });
            },
            error => {
                this.setState({
                    content:
                        (error.response && error.response.data) ||
                        error.message ||
                        error.toString()
                });
            }
        );

    }



    render() {
        function buttonFormatter(cell, row) {
            return <Link to={{
                pathname: "/updateCustomer",
                state: { customerId: { cell } }
            }}>
                <Button variant="primary">
                    Detail
            </Button>
            </Link>;
        }

        function checkboxFormatter(cell, row) {
            return <input type="checkbox" readOnly
                checked={cell} />
        }

        const options = {
            searchPosition: 'left'
        };

        return (
            <div className="container-fluid" >
                <Headers></Headers>
                <div className="row content">
                    <div className="col-sm-2"></div>
                    <div className="col-sm-8">
                        <BootstrapTable
                            multiColumnSort={2}
                            data={this.state.customerList}
                            pagination
                            options={options}
                            search>
                            <TableHeaderColumn dataSort={true} dataField='id' isKey>ID</TableHeaderColumn>
                            <TableHeaderColumn dataSort={true} dataField='username'>Username</TableHeaderColumn>
                            <TableHeaderColumn dataSort={true} dataField='fullName'>Full Name</TableHeaderColumn>
                            <TableHeaderColumn dataSort={true} dataSort={true} dataField='email'>Email</TableHeaderColumn>
                            <TableHeaderColumn dataSort={true} dataField='expiredDate'>Expired Date</TableHeaderColumn>
                            <TableHeaderColumn dataField='enableStatus' dataFormat={checkboxFormatter}>Enable</TableHeaderColumn>
                            <TableHeaderColumn dataField='id' dataFormat={buttonFormatter}>Detail</TableHeaderColumn>
                        </BootstrapTable>
                    </div>
                    <div className="col-sm-2"></div>
                </div>
            </div>
        )
    }
}
