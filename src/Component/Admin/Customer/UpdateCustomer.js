import React, { Component } from 'react'
import CheckButton from "react-validation/build/button"
import Form from "react-validation/build/form"
import Select from "react-validation/build/select"
import Headers from "../Layout/Header"
import CustomerService from "../Service/CustomerService"
import Moment from 'moment';

const required = value => {
    if (!value) {
        return (
            <div className="alert alert-danger" role="alert">
                This field is required!
            </div>
        );
    }
};
export default class UpdateCustomer extends Component {

    constructor(props) {
        super(props);
        this.onChangeUpdateExpiredDate = this.onChangeUpdateExpiredDate.bind(this);
        this.handleUpdateCustomer = this.handleUpdateCustomer.bind(this);
        this.onChangeEnableStatus = this.onChangeEnableStatus.bind(this);
        this.state = {
            id: "",
            password: "",
            email: "",
            fullName: "",
            address: "",
            phoneNumber: "",
            birthday: "",
            gender: "",
            content: [],
            enableStatus: "",
            expiredDate: "",
            addDate: "0",
            successful: false,
            message: ""
        };
    }

    onChangeUpdateExpiredDate(e) {
        this.setState({
            addDate: e.target.value
        });
    }

    onChangeEnableStatus(e) {
        this.setState({
            enableStatus: e.target.value
        });
    }

    handleUpdateCustomer(e) {
        e.preventDefault();

        this.setState({
            message: "",
            successful: false
        });

        this.form.validateAll();
        const exDate = new Date(this.state.expiredDate);
        exDate.setDate(exDate.getDate() + parseInt(this.state.addDate));
        const newDate = new Date(exDate);
        const newExpiredDate = Moment(newDate).format('YYYY-MM-DD');
        console.log(newExpiredDate)
        if (this.checkBtn.context._errors.length === 0) {
            CustomerService.updateCustomer(
                this.state.enableStatus,
                newExpiredDate,
                this.state.id,
                this.state.password,
                this.state.email,
                this.state.fullName,
                this.state.address,
                this.state.phoneNumber,
                this.state.birthday,
                this.state.gender,

            ).then(
                response => {
                    this.setState({
                        message: response.data.message,
                        successful: true
                    });
                },
                error => {
                    const resMessage =
                        (error.response &&
                            error.response.data &&
                            error.response.data.message) ||
                        error.message ||
                        error.toString();

                    this.setState({
                        successful: false,
                        message: resMessage
                    });
                }
            );
        }
    }


    componentDidMount() {
        const { customerId } = this.props.location.state;
        CustomerService.getCustomerDetail(customerId.cell).then(
            response => {
                this.setState({
                    content: [response.data],
                    expiredDate: response.data.expiredDate,
                    enableStatus: response.data.enableStatus,
                    id: response.data.id,
                    password: response.data.password,
                    email: response.data.email,
                    fullName: response.data.fullName,
                    address: response.data.address,
                    phoneNumber: response.data.phoneNumber,
                    birthday: response.data.birthday,
                    gender: response.data.gender,
                });
            },
            error => {
                this.setState({
                    content:
                        (error.response && error.response.data) ||
                        error.message ||
                        error.toString()
                });
            }
        );



    }

    render() {
        return (
            <div className="container-fluid">
                <Headers></Headers>
                <div className="row content">
                    <div className="col-sm-2"></div>
                    <div className="col-sm-8">
                        <div className="form-group row">
                            <div className="col-sm-4">
                                Information of Customer
                            </div>
                        </div>
                        {this.state.content.map(d =>

                            <table className="table table-hover">
                                <tbody>
                                    <tr>
                                        <th scope="row">Full Name</th>
                                        <td colSpan="6">{d.fullName}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Address</th>
                                        <td colSpan="6">{d.address}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Phone Number</th>
                                        <td colSpan="6">{d.phoneNumber}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Email</th>
                                        <td colSpan="6">{d.email}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Gender</th>
                                        <td colSpan="6">{d.gender}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Birthday</th>
                                        <td colSpan="6">{d.birthday}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Expired Date</th>
                                        <td colSpan="6">{d.expiredDate}</td>
                                    </tr>

                                </tbody>
                            </table>
                        )}
                        <Form
                            onSubmit={this.handleUpdateCustomer}
                            ref={c => {
                                this.form = c;
                            }}
                        >
                            <div>
                                <div className="form-group row">
                                    <label className="col-md-4 col-form-label" htmlFor="gender"></label>
                                    <div className="col-sm-8">
                                        <Select name='addDate' value={this.state.addDate} validations={[required]} onChange={this.onChangeUpdateExpiredDate}>
                                            <option value="0">Not yet</option>
                                            <option value="30">Monthly</option>
                                            <option value="365">Yealy</option>
                                        </Select>
                                    </div>
                                </div>
                                <div className="form-group row">
                                    <label className="col-md-4 col-form-label" htmlFor="gender">Enable Status: </label>
                                    <div className="col-sm-8">
                                        <Select name='enableStatus' value={this.state.enableStatus} validations={[required]} onChange={this.onChangeEnableStatus}>
                                            <option value="true">Enable</option>
                                            <option value="false">Disable</option>
                                        </Select>
                                    </div>
                                </div>
                                <div className="form-group row">
                                    <div className="col-md-6">
                                        <button >Update</button>
                                    </div>
                                </div>
                            </div>
                            {this.state.message && (
                                <div className="form-group">
                                    <div
                                        className={
                                            this.state.successful
                                                ? "alert alert-success"
                                                : "alert alert-danger"
                                        }
                                        role="alert"
                                    >
                                        {this.state.message}
                                    </div>
                                </div>
                            )}
                            <CheckButton
                                style={{ display: "none" }}
                                ref={c => {
                                    this.checkBtn = c;
                                }}
                            />
                        </Form>

                    </div>
                    <div className="col-sm-2"></div>
                </div>
            </div>
        )
    }
}
