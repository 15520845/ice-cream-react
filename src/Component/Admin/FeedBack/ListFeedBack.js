import React, { Component } from 'react'
import { Button } from 'react-bootstrap'
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table'
import Headers from "../Layout/Header"
import FAQFeedBackService from "../Service/FAQFeedBackService"
import FAQFeedback from '../FAQ/FAQFeedback'

export default class ListFeedBack extends Component {

    constructor(props) {
        super(props);

        this.state = {
            feedbackList: []
        };
    }

    componentDidMount() {

        FAQFeedBackService.getListFeedBack().then(
            response => {
                this.setState({
                    feedbackList: response.data
                });
            },
            error => {
                this.setState({
                    content:
                        (error.response && error.response.data) ||
                        error.message ||
                        error.toString()
                });
            }
        );
    }

    render() {
        function linkFormatter(cell, row) {
            return <Button variant="primary" onClick={() => {
                FAQFeedBackService.deleteFeedBack(cell).then(
                    response => {
                        window.location.reload();
                    },
                    error => {
                        const resMessage =
                            (error.response &&
                                error.response.data &&
                                error.response.data.message) ||
                            error.message ||
                            error.toString();


                    }
                )
            }}>
                Delete
    </Button>;
        }

        return (
            <div className="container-fluid" >
                <Headers></Headers>
                <FAQFeedback></FAQFeedback>
                <div className="row content">
                    <div className="col-sm-2"></div>
                    <div className="col-sm-8">
                        <BootstrapTable
                            multiColumnSort={2}
                            data={this.state.feedbackList}
                            pagination>
                            <TableHeaderColumn dataSort={true} dataField='id' isKey>ID</TableHeaderColumn>
                            <TableHeaderColumn dataSort={true} dataField='fullName'>Full Name</TableHeaderColumn>
                            <TableHeaderColumn dataSort={true} dataField='title'>Title</TableHeaderColumn>
                            <TableHeaderColumn dataSort={true} dataField='content'>Content</TableHeaderColumn>
                            <TableHeaderColumn dataField='id' dataFormat={linkFormatter}>Delete</TableHeaderColumn>
                        </BootstrapTable>
                    </div>
                    <div className="col-sm-2"></div>
                </div>
            </div>
        )
    }
}
