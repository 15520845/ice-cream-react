import React, { Component } from 'react'
import { Link } from "react-router-dom"
export default class FAQFeedback extends Component {
    render() {
        return (
            <div className="row">
                <Link to={"/listFeedback"} className="nav-link">
                    Feedback
                </Link>|
                <Link to={"/listFaq"} className="nav-link">
                    FAQ
                </Link>|
                <Link to={"/addNewFAQ"} className="nav-link">
                    Add New FAQ
                </Link>
            </div>
        )
    }
}
