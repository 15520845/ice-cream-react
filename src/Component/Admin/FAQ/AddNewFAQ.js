import React, { Component } from 'react'
import CheckButton from "react-validation/build/button"
import Form from "react-validation/build/form"
import Textarea from "react-validation/build/textarea"
import Header from '../Layout/Header'
import FAQFeedBackService from "../Service/FAQFeedBackService"
const required = value => {
    if (!value) {
        return (
            <div className="alert alert-danger" role="alert">
                This field is required!
            </div>
        );
    }
};
export default class AddNewFAQ extends Component {
    constructor(props) {
        super(props);
        this.handlePostFeedBack = this.handlePostFeedBack.bind(this);
        this.onChangeQuestion = this.onChangeQuestion.bind(this);
        this.onChangeAnswer = this.onChangeAnswer.bind(this);

        this.state = {
            question: "",
            answer: "",
            successful: false,
            message: ""
        };
    }


    onChangeQuestion(e) {
        this.setState({
            question: e.target.value
        });
    }

    onChangeAnswer(e) {
        this.setState({
            answer: e.target.value
        });
    }

    handlePostFeedBack(e) {
        e.preventDefault();

        this.setState({
            message: "",
            successful: false
        });

        this.form.validateAll();

        if (this.checkBtn.context._errors.length === 0) {
            FAQFeedBackService.postFQA( this.state.question, this.state.answer).then(
                response => {
                    this.setState({
                        message: "Successful",
                        successful: true
                    });
                },
                error => {
                    const resMessage =
                        (error.response &&
                            error.response.data &&
                            error.response.data.message) ||
                        error.message ||
                        error.toString();

                    this.setState({
                        successful: false,
                        message: resMessage
                    });
                }
            );
        } else {
            this.setState({
                successful: false
            });
        }
    }

    render() {

        return (

            <div className="container-fluid">
                <Header></Header>
                <br></br>
                <h2 className="text-center">FAQ</h2>
                <div className="row content">
                    <div className="col-sm-2"></div>
                    <div className="col-sm-8">
                        <Form
                            onSubmit={this.handlePostFeedBack}
                            ref={c => {
                                this.form = c;
                            }}>

                            <div className="form-group row">
                                <label className="col-sm-4 col-form-label" htmlFor="content">Question: </label>
                                <div className="col-sm-8">
                                    <Textarea
                                        className="form-control"
                                        name="content"
                                        value={this.state.question}
                                        onChange={this.onChangeQuestion}
                                        validations={[required]}
                                    />
                                </div>
                            </div>

                            <div className="form-group row">
                                <label className="col-sm-4 col-form-label" htmlFor="content">Answer: </label>
                                <div className="col-sm-8">
                                    <Textarea
                                        className="form-control"
                                        name="content"
                                        value={this.state.answer}
                                        onChange={this.onChangeAnswer}
                                        validations={[required]}
                                    />
                                </div>
                            </div>

                            <div className="form-group row">
                                <div className="col-sm-6">
                                    <button >
                                        Add
                                    </button>
                                </div>

                            </div>
                            {this.state.message && (
                                <div className="form-group">
                                    <div
                                        className={
                                            this.state.successful
                                                ? "alert alert-success"
                                                : "alert alert-danger"
                                        }
                                        role="alert"
                                    >
                                        {this.state.message}
                                    </div>
                                </div>
                            )}
                            <CheckButton
                                style={{ display: "none" }}
                                ref={c => {
                                    this.checkBtn = c;
                                }}
                            />
                        </Form>
                    </div>
                    <div className="col-sm-2"></div>
                </div>
            </div>
        )
    }
}
