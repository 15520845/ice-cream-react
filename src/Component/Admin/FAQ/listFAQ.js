import React, { Component } from 'react'
import Headers from "../Layout/Header"
import FAQFeedBackService from "../Service/FAQFeedBackService"
import { Link } from "react-router-dom"
import FAQFeedback from './FAQFeedback'
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table'
import { Button } from 'react-bootstrap'

export default class listFAQ extends Component {

    constructor(props) {
        super(props);
        this.delete = this.delete.bind(this);
        this.state = {
            FAQList: []
        };
    }

    componentDidMount() {

        FAQFeedBackService.getListFAQ().then(
            response => {
                this.setState({
                    FAQList: response.data
                });
            },
            error => {
                this.setState({
                    content:
                        (error.response && error.response.data) ||
                        error.message ||
                        error.toString()
                });
            }
        );
    }
    delete(id) {
        FAQFeedBackService.deleteFAQ(id).then(
            response => {
                this.setState({
                    message: "successful",
                    successful: true

                });
                window.location.reload();
            },
            error => {
                const resMessage =
                    (error.response &&
                        error.response.data &&
                        error.response.data.message) ||
                    error.message ||
                    error.toString();

                this.setState({
                    successful: false,
                    message: resMessage
                });
            }
        )
    }
    render() {
        function linkFormatter(cell, row) {
            return <Button variant="primary" onClick={() =>{FAQFeedBackService.deleteFAQ(cell).then(
                response => {
                    window.location.reload();
                },
                error => {
                    const resMessage =
                        (error.response &&
                            error.response.data &&
                            error.response.data.message) ||
                        error.message ||
                        error.toString();
    

                }
            )} }>
            Delete
    </Button>;
        }


        
        return (
            <div className="container-fluid" >
                <Headers></Headers>
                <FAQFeedback></FAQFeedback>
                <div className="row content">
                    <div className="col-sm-2"></div>
                    <div className="col-sm-8">
                        <BootstrapTable
                            multiColumnSort={2}
                            data={this.state.FAQList}
                            pagination>
                            <TableHeaderColumn dataSort={true} dataField='id' isKey>ID</TableHeaderColumn>
                            <TableHeaderColumn dataSort={true} dataField='question'>Question</TableHeaderColumn>
                            <TableHeaderColumn dataSort={true} dataField='answer'>Answer</TableHeaderColumn>
                            <TableHeaderColumn dataField='id' dataFormat={linkFormatter}>Delete</TableHeaderColumn>
                        </BootstrapTable>
                    </div>
                    <div className="col-sm-2"></div>
                </div>
            </div>
        )
    }
}
