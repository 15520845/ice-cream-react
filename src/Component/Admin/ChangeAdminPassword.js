import React, { Component } from 'react'

export default class ChangeAdminPassword extends Component {
    constructor(props) {
        super(props);
        this.handleChangePassword = this.handleChangePassword.bind(this);
        this.onChangeNewPassword = this.onChangeNewPassword.bind(this);
        this.onChangePassword = this.onChangePassword.bind(this);
        this.onChangeRetypeNewPassword = this.onChangeRetypeNewPassword.bind(this);

        this.state = {
            currentUser: AuthService.getCurrentUser(),
            newPassword: "",
            password: "",
            retypeNewPassword: "",
            successful: false,
            loading: false,
            message: ""
        };
    }

    onChangeNewPassword(e) {
        this.setState({
            newPassword: e.target.value
        });
    }

    onChangePassword(e) {
        this.setState({
            password: e.target.value
        });
    }

    onChangeRetypeNewPassword(e) {

        this.setState({
            retypeNewPassword: e.target.value
        });
    }

    handleChangePassword(e) {
        e.preventDefault();

        this.setState({
            message: "",
            loading: true
        });

        this.form.validateAll();
        if (this.state.retypeNewPassword != this.state.newPassword) {
            console.error("Not match");
            this.checkBtn.context._errors = " Not match";
            this.setState({
                message: "Not Match",
                loading: false
            });
        }
console.log(this.state.currentUser.id)
        if (this.checkBtn.context._errors.length === 0) {
            AuthService.changePassword(this.state.password, this.state.newPassword, this.state.currentUser.id).then(
                response => {
                    this.setState({
                        message: response.data.message,
                        successful: true,
                        loading:true
                    });
                },
                error => {
                    const resMessage =
                        (error.response &&
                            error.response.data &&
                            error.response.data.message) ||
                        error.message ||
                        error.toString();

                    this.setState({
                        successful: false,
                        loading: false,
                        message: resMessage
                    });
                }
            );
        } else {
            this.setState({
                loading: false,
                successful: false
            });
        }
    }

    render() {
        return (
            <div className="col-sm-12">
                <br></br>
                <p>Login</p>
                <Form
                    onSubmit={this.handleChangePassword}
                    ref={c => {
                        this.form = c;
                    }}>
                    <div className="form-group row">
                        <label className="col-sm-4 col-form-label" htmlFor="username">Old Password</label>
                        <div className="col-sm-8">
                            <Input
                                type="password"
                                placeholder="Password"
                                className="form-control"
                                name="password"
                                value={this.state.password}
                                onChange={this.onChangePassword}
                                validations={[required]}
                            />
                        </div>
                    </div>

                    <div className="form-group row">
                        <label className="col-sm-4 col-form-label" htmlFor="password">New Password</label>
                        <div className="col-sm-8">
                            <Input
                                type="password"
                                placeholder="Password"
                                className="form-control"
                                name="newPassword"
                                value={this.state.newpassword}
                                onChange={this.onChangeNewPassword}
                                validations={[required]}
                            />
                        </div>
                    </div>

                    <div className="form-group row">
                        <label className="col-sm-4 col-form-label" htmlFor="password">Re-type New Password</label>
                        <div className="col-sm-8">
                            <Input
                                type="password"
                                className="form-control"
                                name="retypeNewPassword"
                                value={this.state.retypeNewPassword}
                                onChange={this.onChangeRetypeNewPassword}
                                validations={[required]}
                            />
                        </div>
                    </div>

                    <div className="form-group row">
                        <div className="col-sm-12">
                            <button className="btn btn-primary btn-block" disabled={this.state.loading}>
                                {this.state.loading && (
                                    <span className="spinner-border spinner-border-sm"></span>
                                )}
                                <span>Change Password</span>
                            </button>
                        </div>

                    </div>
                    {this.state.message && (
                        <div className="form-group">
                            <div
                                className={
                                    this.state.successful
                                        ? "alert alert-success"
                                        : "alert alert-danger"
                                }
                                role="alert">
                                {this.state.message}
                            </div>
                        </div>
                    )}
                    <CheckButton
                        style={{ display: "none" }}
                        ref={c => {
                            this.checkBtn = c;
                        }}
                    />
                </Form>
            </div>
        )
    }
}
