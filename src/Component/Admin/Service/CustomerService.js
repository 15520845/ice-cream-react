import axios from "axios";
import authHeader from './AdminHeadAuth';

class CustomerService {

    getListCustomer() {
        return axios.get("http://localhost:8080/customer/", { headers: authHeader() });
    }

    getCustomerDetail(id) {
        return axios.get("http://localhost:8080/customer/" + id, { headers: authHeader() });
    }

    updateCustomer(enableStatus, expiredDate, id, password, email,
        fullName,
        address,
        phoneNumber,
        birthday,
        gender) {
        return axios.patch("http://localhost:8080/customer/" + id, {
            enableStatus,
            expiredDate,
            password,
            email,
            fullName,
            address,
            phoneNumber,
            birthday,
            gender
        },
            { headers: authHeader() });

    }
}
export default new CustomerService();