import axios from "axios"
import authHeader from '../../Customer/Service/HeadAuth'

const API_URL = "http://localhost:8080/admin";
class AdminAuthorService {

  login(username, password) {
    return axios
      .post(API_URL + "/login", {
        username,
        password
      })
      .then(response => {
        if (response.data.accessToken) {
          localStorage.setItem("admin", JSON.stringify(response.data));
        }

        return response.data;
      });
  }

  logout() {
    localStorage.removeItem("admin");
  }

  

  changePassword(password, newPassword, id) {
    return axios.patch(API_URL + "/updatePassword/" + id, {
      password,
      newPassword
    },
      { headers: authHeader() });
  }

  updateProfile(email, fullname, address, phoneNumber, birthday, gender, avatar, id) {
    return axios.patch(API_URL + "/" + id, {
      email,
      fullname,
      address,
      phoneNumber,
      birthday,
      gender,
      avatar
    },
      { headers: authHeader() });
  }

  getCurrentUser() {
    return JSON.parse(localStorage.getItem('admin'));;
  }
}

export default new AdminAuthorService();