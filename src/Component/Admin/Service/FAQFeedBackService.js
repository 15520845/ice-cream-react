import axios from "axios";
import authHeader from './AdminHeadAuth';

class FAQFeedBackService {

   
    deleteFAQ(id){
        return axios.delete("http://localhost:8080/faq/"+ id,
        { headers: authHeader() }
         );
    }

    deleteFeedBack(id){
        return axios.delete("http://localhost:8080/feedback/"+ id,
        { headers: authHeader() }
         );
    }

    getListFeedBack(){
        return axios.get("http://localhost:8080/feedback" , { headers: authHeader() });
    }

    getListFAQ(){
        return axios.get("http://localhost:8080/faq" , { headers: authHeader() });
    }


    postFQA(question, answer){
        return axios.post("http://localhost:8080/faq",{
            question, answer
        } ,
        { headers: authHeader() });
    }
   
}
export default new FAQFeedBackService();