
// import './Header.css'
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom"
import React, { Component } from 'react'
import AuthService from '../Service/AdminAuthorService'


export default class Header extends Component {

    constructor(props) {
        super(props);
        this.logOut = this.logOut.bind(this);

        this.state = {
            currentUser: undefined
        };
    }

    componentDidMount() {
        const user = AuthService.getCurrentUser();

        if (user) {
            this.setState({
                currentUser: AuthService.getCurrentUser(),
            });
        }
    }

    logOut() {
        AuthService.logout();
    }

    render() {

        const { currentUser } = this.state;
        return (
            <div>
                <nav className="navbar navbar-expand-lg navbar-dark bg-dark">

                    <a className="navbar-brand" href="#">
                        <img src="/image/images/logo.png" width="90" height="70" alt="" />
                    </a>

                    <div className="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul className="navbar-nav mr-auto">
                            <li className="nav-item">
                                <Link to={"/listCustomer"} className="nav-link">
                                    Customer
                                </Link>
                            </li>
                            <li className="nav-item">
                                <Link to={"/listRecipe"} className="nav-link">
                                    Recipe
                                </Link>
                            </li>
                            <li className="nav-item">
                                <Link to={"/admin/order"} className="nav-link">
                                    Order
                                </Link>
                            </li>
                            <li className="nav-item">
                                <Link to={"/FAQFeedback"} className="nav-link">
                                    FeedbackFAQ
                                </Link>
                            </li>
                            <li className="nav-item">
                                <Link to={"/admin/prize"} className="nav-link">
                                    Prize
                                </Link>
                            </li>
                            <li className="nav-item">
                                <Link to={"/admin/setting"} className="nav-link">
                                    Setting
                                </Link>
                            </li>
                        </ul>
                        {currentUser &&
                            <div className="navbar-nav ml-auto">

                                <li className="nav-item">
                                    <a href="/admin" className="nav-link" onClick={this.logOut}>
                                        LogOut
                                        </a>
                                </li>
                            </div>
                        }

                    </div>
                </nav>

                <div className="container mt-3">

                </div>
            </div>
        )
    }
}

