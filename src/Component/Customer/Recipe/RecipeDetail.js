import React, { Component } from 'react'
import { Media } from 'react-bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css';
import CustomerService from '../Service/CustomerService'
import { BrowserRouter as Router, NavLink, Route, Link } from "react-router-dom"
import Header from '../Layout/Header'
export default class RecipeDetail extends Component {

    constructor(props) {
        super(props);

        this.state = {
            content: [],
            recipeList: []
        };
    }

    componentWillReceiveProps = (nextProps)=> {
        if (nextProps.location.state !== this.props.location.state) {
            window.location.reload();
        }
    };

    componentDidMount() {
        const { recipeId } = this.props.location.state;
        console.log(recipeId)
        
        CustomerService.getRecipeDetail(recipeId).then(
            response => {
                this.setState({
                    content: [response.data]
                });
            },
            error => {
                this.setState({
                    content:
                        (error.response && error.response.data) ||
                        error.message ||
                        error.toString()
                });
            }
        );
        CustomerService.getListRecipe().then(
            response => {
                this.setState({
                    recipeList: response.data
                });
            },
            error => {
                this.setState({
                    content:
                        (error.response && error.response.data) ||
                        error.message ||
                        error.toString()
                });
            }
        );

    }

    render() {
        return (
            <div>
                <Header></Header>
                <br></br>
                <div className="container-fluid">
                    <div className="row content">
                        <div className="col-sm-3 sidenav">
                            <h4>Recipe</h4>
                            <ul className=" ">
                                {this.state.recipeList.map(data =>
                                    <li >
                                        <Link to={{
                                            pathname: "/recipeDetail",
                                            state: {
                                                recipeId: data.recipeId
                                            }
                                        }} className="nav-link">
                                            {data.name}

                                        </Link>
                                    </li>
                                )}
                            </ul><br></br>
                        </div>

                        <div className="col-sm-9">
                            <h4><span>Detail</span></h4>
                            <hr></hr>
                            {this.state.content.map(d =>

                                <Media>
                                    <img
                                        width={200}
                                        height={200}
                                        className="mr-3"
                                        src={"/image/" + d.image}
                                        alt="Generic placeholder"
                                    />
                                    <Media.Body>
                                        <p>
                                            {d.description}
                                        </p>
                                        <br></br>
                                        <hr></hr>
                                        <p>{d.details}</p>
                                        <hr></hr>
                                    </Media.Body>
                                </Media>
                            )}                       
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
