import React, {useState,useEffect} from 'react'
import '../../../App.css'
import axios from 'axios'
import {Container, Col, Card} from 'react-bootstrap'
import Slider from "react-slick";
import "slick-carousel/slick/slick.css"; 
import "slick-carousel/slick/slick-theme.css"
import authHeader from '../Service/HeadAuth.js'

function SampleArrow(props) {
    const { className, style, onClick } = props;
    return (
      <div
        className={className}
        style={{ ...style, display: "block", background: "red", height:"35px", width:"35px", borderRadius:"100px", paddingTop:"8px"}}
        onClick={onClick}
      />
    );
  }

function ListOfRecipe() {

    const [stateRecipe, setRecipestate] = useState([])

    useEffect(() => {
        getRecipe();
    }, [])

    const getRecipe = () =>{
        axios.get("http://localhost:8080/recipe/",{ headers: authHeader() }).then(data=>{
            setRecipestate(data.data);
        }).catch(err=>alert(err));
    }
    const settings = {
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 4,
        slidesToScroll: 1,
        nextArrow: <SampleArrow/>,
        prevArrow: <SampleArrow/>
      };

    return (
        <Container>
            <Slider {...settings}>
                {stateRecipe.map(function (stateRecipe){
                    return (
                        <React.Fragment key={stateRecipe.recipeId}>
                            <Col >
                                <Card >
                                    <Card.Img variant="top" src={"/image/" + stateRecipe.image}  >
                                    </Card.Img>
                                    <Card.Body>
                                       {stateRecipe.name}
                                    </Card.Body>
                                </Card>
                            </Col>
                        </React.Fragment>
                    );
                })}
            </Slider>
        </Container>
    )
}

export default ListOfRecipe
