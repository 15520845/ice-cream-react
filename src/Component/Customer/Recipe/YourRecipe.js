import React, { Component } from 'react'
import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import CheckButton from "react-validation/build/button";
import Textarea from "react-validation/build/textarea";
import CustomerService from "../Service/CustomerService"
import { Link } from 'react-router-dom';
import AuthService from '../Service/AuthService'
import Header from '../Layout/Header'

const required = value => {
    if (!value) {
        return (
            <div className="alert alert-danger" role="alert">
                This field is required!
            </div>
        );
    }
};

export default class YourRecipe extends Component {

    constructor(props) {
        super(props);
        this.handlePostRecipe = this.handlePostRecipe.bind(this);
        this.onChangeName = this.onChangeName.bind(this);
        this.onChangeDescription = this.onChangeDescription.bind(this);
        this.onChangeDetail = this.onChangeDetail.bind(this);
        this.fileChangedHandler = this.fileChangedHandler.bind(this);

        this.state = {
            author: AuthService.getCurrentUser().username,
            name: "",
            image: "",
            details: "",
            description: "",
            successful: false,
            message: ""
        };
    }

    onChangeName(e) {
        this.setState({
            name: e.target.value
        });
    }

    onChangeDescription(e) {
        this.setState({
           description : e.target.value
        });
    }

    onChangeDetail(e) {
        this.setState({
           details : e.target.value
        });
    }

    handlePostRecipe(e) {
        e.preventDefault();

        this.setState({
            message: "",
            successful: true
        });

        this.form.validateAll();

        if (this.checkBtn.context._errors.length === 0) {
            CustomerService.postYourRecip(this.state.name, this.state.image, this.state.description, this.state.details, this.state.author).then(
                
                response => {
                    this.setState({
                        message: response.data.message,
                        successful: true
                    });
                },
                error => {
                    const resMessage =
                        (error.response &&
                            error.response.data &&
                            error.response.data.message) ||
                        error.message ||
                        error.toString();

                    this.setState({
                        successful: false,
                        message: resMessage
                    });
                }
            );
        } else {
            this.setState({
                successful: false
            });
        }
    }

    fileChangedHandler(e) {
        this.setState({
            image: "images/" + e.target.files[0].name
        })

        let reader = new FileReader();

        reader.onloadend = () => {
            this.setState({
                imagePreviewUrl: reader.result
            });
        }

        reader.readAsDataURL(e.target.files[0])

    }

    render() {

        let $imagePreview = (<div className="previewText image-container"></div>);
        if (this.state.imagePreviewUrl) {
            $imagePreview = (<div className="image-container" ><img src={this.state.imagePreviewUrl} alt="icon" width="200" /> </div>);
        }

        return (
            <div>
                <Header></Header>
                <br></br>
                <h2>Your Recipe</h2>
                <Form
                    onSubmit={this.handlePostRecipe}
                    ref={c => {
                        this.form = c;
                    }}>
                    <div className="form-group row">
                        <label className="col-sm-4 col-form-label" htmlFor="name">Name of your recipe</label>
                        <div className="col-sm-8">
                            <Input
                                type="text"
                                className="form-control"
                                name="name"
                                value={this.state.name}
                                onChange={this.onChangeName}
                                validations={[required]}
                            />
                        </div>
                    </div>

                    <div className="form-group row">
                        <label className="col-md-4 col-form-label" htmlFor="avatar">Avatar</label>
                        <div className="col-sm-8">
                            <input type="file" name="avatar" onChange={this.fileChangedHandler} />

                        </div>
                    </div>
                    <div className="form-group row">
                        {$imagePreview}
                    </div>

                    <div className="form-group row">
                        <label className="col-sm-4 col-form-label" htmlFor="description">Description</label>
                        <div className="col-sm-8">
                            <Textarea
                                className="form-control"
                                name="description"
                                value={this.state.description}
                                onChange={this.onChangeDescription}
                                validations={[required]}
                            />
                        </div>
                    </div>

                    <div className="form-group row">
                        <label className="col-sm-4 col-form-label" htmlFor="details">Details</label>
                        <div className="col-sm-8">
                            <Textarea
                                className="form-control"
                                name="details"
                                value={this.state.details}
                                onChange={this.onChangeDetail}
                                validations={[required]}
                            />
                        </div>
                    </div>

                    <div className="form-group row">
                        <div className="col-sm-6">
                            <button className="btn btn-primary btn-block">
                                <span>Upload Recipe</span>
                            </button>
                        </div>
                        <div className="col-sm-6">
                            <Link to={"/home"}>
                            <button>
                                <span>Back</span>
                            </button>
                            </Link>
                        </div>
                    </div>

                    {this.state.message && (
                        <div className="form-group">
                            <div
                                className={
                                    this.state.successful
                                        ? "alert alert-success"
                                        : "alert alert-danger"
                                }
                                role="alert"
                            >
                                {this.state.message}
                            </div>
                        </div>
                    )}
                    <CheckButton
                        style={{ display: "none" }}
                        ref={c => {
                            this.checkBtn = c;
                        }}
                    />
                </Form>
            </div>
        )
    }
}
