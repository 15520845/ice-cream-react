import React, { useState, useEffect } from 'react'
import { Container, Col, Card, Button } from 'react-bootstrap'
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css"
import axios from 'axios'
import authHeader from '../Service/HeadAuth.js'
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom"

function SampleArrow(props) {
    const { className, style, onClick } = props;
    return (
        <div
            className={className}
            style={{ ...style, display: "block", background: "red", height: "35px", width: "35px", borderRadius: "100px", paddingTop: "8px" }}
            onClick={onClick}
        />
    );
}

function SlideRecipe() {

    const [stateRecipe1, setRecipestate1] = useState([])

    useEffect(() => {
        getRecipe();
    }, [])

    const getRecipe = () => {
        axios.get("http://localhost:8080/recipe/", { headers: authHeader() }).then(data => {
            setRecipestate1(data.data);
        }).catch(err => alert(err));
    }

    const settings = {
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        nextArrow: <SampleArrow />,
        prevArrow: <SampleArrow />
    };

    return (
        <Container>
            <Slider {...settings}>
                {stateRecipe1.map(function (stateRecipe1) {
                    return (
                        <React.Fragment key={stateRecipe1.recipeId} >
                            <Card style={{ width: '100%', height: '450px' }}>
                                <Card.Img variant="top" src={"/image/" + stateRecipe1.image} width="50px" height="200px" />
                                <Card.Body >
                                    <Card.Title>{stateRecipe1.name}</Card.Title>
                                    <Card.Text>{stateRecipe1.description}</Card.Text>
                                    <Link to={{
                                        pathname: "/recipeDetail",
                                        state: { recipeId: stateRecipe1.recipeId }
                                    }}>
                                        <Button variant="primary">
                                            View this recipe
                                        </Button>
                                    </Link>
                                </Card.Body>
                            </Card>
                        </React.Fragment>
                    );
                })}
            </Slider>
        </Container>
    )
}

export default SlideRecipe
