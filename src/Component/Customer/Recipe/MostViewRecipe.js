import React, {useState,useEffect} from 'react'
import '../../../App.css'
import axios from 'axios'
import authHeader from '../Service/HeadAuth.js'

function MostViewRecipe() {
    const [stateMostViewRecipe, setMostViewRecipestate] = useState([])

    useEffect(() => {
        getMostViewRecipe();
    }, [])

    const getMostViewRecipe = () =>{
        axios.get("http://localhost:8080/recipe/mostView",{ headers: authHeader() }).then(data=>{
        setMostViewRecipestate(data.data);
        }).catch(err=>alert(err));
    }
    
    return (
       
        <div className="scrollable">
            <table className="table table-hover">
                <thead>
                    <tr>
                         <th colSpan= "4" scope="col" >Most View Recipe</th>
                    </tr>
                </thead>
                <tbody>
                    {stateMostViewRecipe.map(d =>
                    
                        <tr key={d.recipeId}>
                        <th scope="row">{d.recipeId}</th>
                        <td colSpan="3">{d.name}</td>
                    </tr>
                     )}
                    
                </tbody>
            </table>
        </div>
    )
}

export default MostViewRecipe
