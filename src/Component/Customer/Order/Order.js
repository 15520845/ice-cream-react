import React, { Component } from 'react'
import { Media } from 'react-bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css';
import Header from '../Layout/Header'
export default class Order extends Component {
    render() {
        return (
            <div>
                <Header></Header>
                <br></br>
                <Media>
                    <img
                        width={64}
                        height={64}
                        className="mr-6"
                        src="holder.js/64x64"
                        alt="Generic placeholder"
                    />
                    <Media.Body>
                        <p>
                            Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque
                            ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at,
                            tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla.
                            Donec lacinia congue felis in faucibus.
                        </p>
                    </Media.Body>
                </Media>
            </div>
        )
    }
}
