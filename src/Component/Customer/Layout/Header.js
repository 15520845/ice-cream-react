
import './Header.css'
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom"
import React, { Component } from 'react'

import AuthService from '../Service/AuthService'


export default class Header extends Component {

    constructor(props) {
        super(props);
        this.logOut = this.logOut.bind(this);

        this.state = {
            currentUser: undefined
        };
    }

    componentDidMount() {
        const user = AuthService.getCurrentUser();

        if (user) {
            this.setState({
                currentUser: AuthService.getCurrentUser(),
            });
        }
    }

    logOut() {
        AuthService.logout();
    }

    render() {

        const { currentUser } = this.state;
        return (
            <div>
                <nav className="navbar navbar-expand-lg navbar-dark bg-dark">

                    <a className="navbar-brand" href="#">
                        <img src="/image/images/logo.png" width="90" height="70" alt="" />
                    </a>
                    <button className="navbar-toggler ml-auto hidden-sm-up float-xs-right" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon" ></span>
                    </button>

                    <div className="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul className="navbar-nav mr-auto">
                            <li className="nav-item active">
                                <Link to={"/home"} className="nav-link">
                                    Home
                                </Link>
                            </li>
                            <li className="nav-item">
                                <Link to={{
                                    pathname: "/recipeDetail",
                                    state: {recipeId: "1" }
                                    }} className="nav-link">
                                    Recipe
                                </Link>
                            </li>
                            {/* <li className="nav-item">
                                <Link to={"/order"} className="nav-link">
                                    Order
                                    </Link>
                            </li> */}
                        </ul>
                        {currentUser ? (
                            <div className="navbar-nav ml-auto">
                                <li className="nav-item">
                                    <Link to={"/profile"} className="nav-link">
                                        {currentUser.username}
                                    </Link>
                                </li>
                                <li className="nav-item">
                                    <a href="/login" className="nav-link" onClick={this.logOut}>
                                        LogOut
                                        </a>
                                </li>
                            </div>
                        ) : (
                                <div className="navbar-nav ml-auto">
                                    <li className="nav-item">
                                    
                                        <Link to={{
                                            pathname: "/login",
                                            state: {
                                                hasHeader: "true"
                                            }
                                        }} className="nav-link">
                                            Login
                                        </Link>
                                    </li>

                                    <li className="nav-item">
                                        <Link to={"/register"} className="nav-link">
                                            Sign Up
                                        </Link>
                                    </li>
                                </div>
                            )}

                    </div>
                </nav>

                <div className="container mt-3">

                </div>
            </div>
        )
    }
}

