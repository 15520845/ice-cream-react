import React, { Component } from 'react'
import DatePicker from "react-datepicker"
import CheckButton from "react-validation/build/button"
import Form from "react-validation/build/form"
import Input from "react-validation/build/input"
import Select from "react-validation/build/select"
import AuthService from './Service/AuthService'
import "react-datepicker/dist/react-datepicker.css";
import Header from './Layout/Header'

const required = value => {
    if (!value) {
        return (
            <div className="alert alert-danger" role="alert">
                This field is required!
            </div>
        );
    }
};


const vusername = value => {
    if (value.length < 3 || value.length > 20) {
        return (
            <div className="alert alert-danger" role="alert">
                The username must be between 3 and 20 characters.
            </div>
        );
    }
};

const vpassword = value => {
    if (value.length < 6 || value.length > 40) {
        return (
            <div className="alert alert-danger" role="alert">
                The password must be between 6 and 40 characters.
            </div>
        );
    }
};

export default class Register extends Component {

    constructor(props) {
        super(props);
        this.handleRegister = this.handleRegister.bind(this);
        this.onChangeUsername = this.onChangeUsername.bind(this);
        this.onChangeEmail = this.onChangeEmail.bind(this);
        this.onChangePassword = this.onChangePassword.bind(this);
        this.onChangeFullname = this.onChangeFullname.bind(this);
        this.onChangePhoneNumber = this.onChangePhoneNumber.bind(this);
        this.onChangeAddress = this.onChangeAddress.bind(this);
        this.onChangeBirthday = this.onChangeBirthday.bind(this);
        this.onChangeGender = this.onChangeGender.bind(this);
        this.fileChangedHandler = this.fileChangedHandler.bind(this);
        this.handleClear = this.handleClear.bind(this);

        this.state = {
            username: "",
            email: "",
            password: "",
            gender: "1",
            phoneNumber: "",
            fullname: "",
            address: "",
            birthday: "",
            avatar: "",
            successful: false,
            message: ""
        };
    }

    onChangeUsername(e) {
        this.setState({
            username: e.target.value
        });
    }

    onChangeEmail(e) {
        this.setState({
            email: e.target.value
        });
    }

    onChangeFullname(e) {
        this.setState({
            fullname: e.target.value
        });
    }

    onChangeBirthday(e) {
        this.setState({
            birthday: e
        });
    }

    onChangePhoneNumber(e){
        this.setState({
            phoneNumber: e.target.value
        });
    }

    onChangeAddress(e){
        this.setState({
            address: e.target.value
        });
    }

    onChangePassword(e) {
        this.setState({
            password: e.target.value
        });
    }

    onChangeGender(e) {
        this.setState({
            gender: e.target.value
        });
    }

    fileChangedHandler(e) {
        this.setState({
            avatar: "image/images/uploaded/" + e.target.files[0].name
        })
        let reader = new FileReader();

        reader.onloadend = () => {
            this.setState({
                imagePreviewUrl: reader.result
            });
        }

        reader.readAsDataURL(e.target.files[0])

    }

    handleRegister(e) {
        e.preventDefault();

        this.setState({
            message: "",
            successful: false
        });

        this.form.validateAll();

        if (this.checkBtn.context._errors.length === 0) {
            AuthService.register(
                this.state.username,
                this.state.email,
                this.state.password,
                this.state.fullname,
                this.state.address,
                this.state.phoneNumber,
                this.state.birthday,
                this.state.gender,
                this.state.avatar
            ).then(
                response => {
                    this.setState({
                        message: response.data.message,
                        successful: true
                    });
                },
                error => {
                    const resMessage =
                        (error.response &&
                            error.response.data &&
                            error.response.data.message) ||
                        error.message ||
                        error.toString();

                    this.setState({
                        successful: false,
                        message: resMessage
                    });
                }
            );
        }
    }

    handleClear (){
        this.setState({
            username: "",
            password: "",
            gender: "",
            phoneNumber: "",
            fullname: "",
            address: "",
            birthday: "",
            avatar: "",
            email: ""
        });
    }
    
    render() {

        let $imagePreview = (<div className="previewText image-container"></div>);
        if (this.state.imagePreviewUrl) {
            $imagePreview = (<div className="image-container" ><img src={this.state.imagePreviewUrl} alt="icon" width="200" /> </div>);
        }

        return (
            <div className="col-md-12">
                <Header></Header>
                <Form
                    onSubmit={this.handleRegister}
                    ref={c => {
                        this.form = c;
                    }}
                >
                    {!this.state.successful && (
                        <div>
                            <div className="form-group row">
                                <label className="col-md-4 col-form-label" htmlFor="username">Username</label>
                                <div className="col-sm-8">
                                    <Input
                                        type="text"
                                        className="form-control"
                                        name="username"
                                        value={this.state.username}
                                        onChange={this.onChangeUsername}
                                        validations={[required, vusername]}
                                    />
                                </div>
                            </div>

                            <div className="form-group row">
                                <label className="col-md-4 col-form-label" htmlFor="password">Password</label>
                                <div className="col-sm-8">
                                    <Input
                                        type="password"
                                        className="form-control"
                                        name="password"
                                        value={this.state.password}
                                        onChange={this.onChangePassword}
                                        validations={[required, vpassword]}
                                    />
                                </div>
                            </div>

                            <div className="form-group row">
                                <label className="col-md-4 col-form-label" htmlFor="fullname">Fullname</label>
                                <div className="col-sm-8">
                                    <Input
                                        type="text"
                                        className="form-control"
                                        name="fullname"
                                        value={this.state.fullname}
                                        onChange={this.onChangeFullname}
                                        validations={[required]}
                                    />
                                </div>
                            </div>

                            <div className="form-group row">
                                <label className="col-md-4 col-form-label" htmlFor="address">Address</label>
                                <div className="col-sm-8">
                                    <Input
                                        type="text"
                                        className="form-control"
                                        name="address"
                                        value={this.state.address}
                                        onChange={this.onChangeAddress}
                                        validations={[required]}
                                    />
                                </div>
                            </div>

                            <div className="form-group row">
                                <label className="col-md-4 col-form-label" htmlFor="phonenumber">Phone Number</label>
                                <div className="col-sm-8">
                                    <Input
                                        type="Number"
                                        className="form-control"
                                        name="phonenumber"
                                        value={this.state.phoneNumber}
                                        onChange={this.onChangePhoneNumber}
                                        validations={[required]}
                                    />
                                </div>
                            </div>

                            <div className="form-group row">
                                <label className="col-md-4 col-form-label" htmlFor="email">Email</label>
                                <div className="col-sm-8">
                                    <Input
                                        type="text"
                                        className="form-control"
                                        name="email"
                                        value={this.state.email}
                                        onChange={this.onChangeEmail}
                                        validations={[required]}
                                    />
                                </div>
                            </div>
                            <div className="form-group row">
                                <label className="col-md-4 col-form-label" htmlFor="gender">Gender</label>
                                <div className="col-sm-8">
                                    <Select name='city' value={this.state.gender} validations={[required]} onChange= {this.onChangeGender}>
                                        <option value='1'>Male</option>
                                        <option value='0'>Female</option>
                                    </Select>
                                </div>
                            </div>
                            <div className="form-group row">
                                <label className="col-md-4 col-form-label" htmlFor="birthday">Birthday</label>
                                <div className="col-sm-8">
                                    <DatePicker
                                        className="form-control"
                                        name="birthday"
                                        selected={this.state.birthday}
                                        onChange={this.onChangeBirthday}
                                    />
                                </div>
                            </div>

                            <div className="form-group row">
                                <label className="col-md-4 col-form-label" htmlFor="avatar">Avatar</label>
                                <div className="col-sm-8">
                                    <input type="file" name="avatar" onChange={this.fileChangedHandler} />
                                </div>
                            </div>
                            <div className="form-group row">
                                {$imagePreview}
                            </div>


                            <div className="form-group row">
                                <div className="col-md-6">
                                    <button className="btn btn-primary btn-block">Sign Up</button>
                                </div>
                                <div className="col-md-6">
                                    <button onClick={this.handleClear}>Clear</button>
                                </div>
                            </div>
                        </div>
                    )}

                    {this.state.message && (
                        <div className="form-group">
                            <div
                                className={
                                    this.state.successful
                                        ? "alert alert-success"
                                        : "alert alert-danger"
                                }
                                role="alert"
                            >
                                {this.state.message}
                            </div>
                        </div>
                    )}
                    <CheckButton
                        style={{ display: "none" }}
                        ref={c => {
                            this.checkBtn = c;
                        }}
                    />
                </Form>
            </div>
        );
    }
}
