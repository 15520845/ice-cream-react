import React from 'react';
import Login from './Login';
import MostViewRecipe from './Recipe/MostViewRecipe';
import SlideRecipe from './Recipe/SlideRecipe';
import UserRecipe from './UserRecipe/UserRecipe';
import ListOfRecipe from './Recipe/ListOfRecipe';
import Header from './Layout/Header'
import { Link } from "react-router-dom"

function HomePage() {
    return (
        <div>
            <Header></Header>
            <div className="container text-center">
                <br></br>
                <div className="row">
                    <div className="col-sm-12">
                        <SlideRecipe></SlideRecipe>
                    </div>
                </div>
                <div className="row">
                    <div className="col-sm-4">
                        <UserRecipe></UserRecipe>
                    </div>
                    <div className="col-sm-4">
                        <MostViewRecipe></MostViewRecipe>
                    </div>
                    <div className="col-sm-4 border">
                        <div className="well">
                            <Login ></Login>
                        </div>
                    </div>
                </div>
                <br></br>
                <div className="row">
                    <div className="col-sm-12">
                        <ListOfRecipe></ListOfRecipe>
                    </div>
                </div>
                <br></br>
                <div className="row">
                    <div className="col-sm-4"></div>
                    <div className="col-sm-4">
                        <div className="row">
                            <Link to={"/faq"} className="nav-link">FAQ</Link>|<Link to={"/feedback"} className="nav-link">Feedback</Link>|<Link to={"/about"} className="nav-link">About Us</Link>|<Link to={"/contact"} className="nav-link">Contact Us</Link>
                        </div>
                    </div>
                    <div className="col-sm-4"></div>
                </div>
            </div>
            <br></br>
        </div>
    )
}

export default HomePage
