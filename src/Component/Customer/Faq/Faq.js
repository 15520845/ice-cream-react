import React, { Component } from 'react'
import CustomerService from '../Service/CustomerService'
import Header from '../Layout/Header'
export default class Faq extends Component {
    constructor(props) {
        super(props);

        this.state = {
            content: []
        };
    }

    componentDidMount() {
        CustomerService.getFaq().then(
            response => {
                this.setState({
                    content: response.data
                });
            },
            error => {
                this.setState({
                    content:
                        (error.response && error.response.data) ||
                        error.message ||
                        error.toString()
                });
            }
        );
    }

    render() {
        console.log(this.state.content)
        return (
            <div className="container-fluid">
                <Header></Header>
                <div className="row content">
                    <div className="col-sm-2"></div>
                    <div className="col-sm-8">
                        <table className="table table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th colSpan="4" scope="col" >Fequently Asked Question</th>
                                </tr>
                            </thead>
                            {this.state.content.map(data =>
                                <tbody>

                                    <tr>
                                        <td ><span>Question:</span> {data.question}</td>
                                    </tr>
                                    <tr>
                                        <td> <span>Answer: </span>{data.answer}</td>
                                    </tr>
                                </tbody>
                            )}
                        </table>
                    </div>
                    <div className="col-sm-2"></div>
                </div>
            </div>
        )
    }
}
