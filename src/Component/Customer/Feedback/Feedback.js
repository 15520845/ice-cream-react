import React, { Component } from 'react'
import CheckButton from "react-validation/build/button"
import Form from "react-validation/build/form"
import Input from "react-validation/build/input"
import CustomerService from '../Service/CustomerService'
import Textarea from "react-validation/build/textarea";
import Header from '../Layout/Header'
const required = value => {
    if (!value) {
        return (
            <div className="alert alert-danger" role="alert">
                This field is required!
            </div>
        );
    }
};
export default class Feedback extends Component {
    constructor(props) {
        super(props);
        this.handlePostFeedBack = this.handlePostFeedBack.bind(this);
        this.onChangeFullName = this.onChangeFullName.bind(this);
        this.onChangeTitle = this.onChangeTitle.bind(this);
        this.onChangeContent = this.onChangeContent.bind(this);

        this.state = {
            fullname: "",
            title: "",
            content: "",
            successful: false,
            message: ""
        };
    }

    onChangeFullName(e) {
        this.setState({
            fullname: e.target.value
        });
    }

    onChangeTitle(e) {
        this.setState({
            title: e.target.value
        });
    }

    onChangeContent(e) {
        this.setState({
            content: e.target.value
        });
    }

    handlePostFeedBack(e) {
        e.preventDefault();

        this.setState({
            message: "",
            successful: false
        });

        this.form.validateAll();

        if (this.checkBtn.context._errors.length === 0) {
            CustomerService.postFeedBack(this.state.fullname, this.state.title, this.state.content).then(
                response => {
                    this.setState({
                        message: response.data.message,
                        successful: true
                    });
                },
                error => {
                    const resMessage =
                        (error.response &&
                            error.response.data &&
                            error.response.data.message) ||
                        error.message ||
                        error.toString();

                    this.setState({
                        successful: false,
                        message: resMessage
                    });
                }
            );
        } else {
            this.setState({
                successful: false
            });
        }
    }

    handleClear() {
        this.setState({
            fullname: "",
            title: "",
            content: ""
        });
    }
    render() {

        let $imagePreview = (<div className="previewText image-container"></div>);
        if (this.state.imagePreviewUrl) {
            $imagePreview = (<div className="image-container" ><img src={this.state.imagePreviewUrl} alt="icon" width="200" /> </div>);
        }

        return (

            <div className="container-fluid">
                <Header></Header>
                <br></br>
                <h2 className="text-center">FeedBack</h2>
                <div className="row content">
                    <div className="col-sm-2"></div>
                    <div className="col-sm-8">
                        <Form
                            onSubmit={this.handlePostFeedBack}
                            ref={c => {
                                this.form = c;
                            }}>
                            <div className="form-group row">
                                <label className="col-sm-4 col-form-label" htmlFor="fullname">Full Name: </label>
                                <div className="col-sm-8">
                                    <Input
                                        type="text"
                                        className="form-control"
                                        name="fullname"
                                        value={this.state.fullname}
                                        onChange={this.onChangeFullName}
                                        validations={[required]}
                                    />
                                </div>
                            </div>

                            <div className="form-group row">
                                <label className="col-sm-4 col-form-label" htmlFor="title">Title: </label>
                                <div className="col-sm-8">
                                    <Input
                                        type="text"
                                        className="form-control"
                                        name="title"
                                        value={this.state.title}
                                        onChange={this.onChangeTitle}
                                        validations={[required]}
                                    />
                                </div>
                            </div>

                            <div className="form-group row">
                                <label className="col-sm-4 col-form-label" htmlFor="content">Content: </label>
                                <div className="col-sm-8">
                                    <Textarea
                                        className="form-control"
                                        name="content"
                                        value={this.state.content}
                                        onChange={this.onChangeContent}
                                        validations={[required]}
                                    />
                                </div>
                            </div>

                            <div className="form-group row">
                                <div className="col-sm-4">
                                    <button >
                                        Submit
                                    </button>
                                </div>

                                <div className="col-sm-4">
                                    <button onClick={this.handleClear}>Reset</button>
                                </div>
                                <div className="col-sm-4"> </div>
                            </div>
                            {this.state.message && (
                                <div className="form-group">
                                    <div
                                        className={
                                            this.state.successful
                                                ? "alert alert-success"
                                                : "alert alert-danger"
                                        }
                                        role="alert"
                                    >
                                        {this.state.message}
                                    </div>
                                </div>
                            )}
                            <CheckButton
                                style={{ display: "none" }}
                                ref={c => {
                                    this.checkBtn = c;
                                }}
                            />
                        </Form>
                    </div>
                    <div className="col-sm-2"></div>
                </div>
            </div>
        )
    }
}
