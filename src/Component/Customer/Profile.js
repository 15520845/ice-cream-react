import React, { Component } from 'react'
import { Button } from 'react-bootstrap';
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom"
import CustomerService from './Service/CustomerService'
import AuthService from './Service/AuthService'
import Header from './Layout/Header'
export default class Profile extends Component {

    constructor(props) {
        super(props);

        this.state = {
            username: "",
            content: []
        };
    }

    componentDidMount() {

        CustomerService.getProfile(AuthService.getCurrentUser().username).then(
            response => {
                this.setState({
                    content: [response.data]
                });
            },
            error => {
                this.setState({
                    content:
                        (error.response && error.response.data) ||
                        error.message ||
                        error.toString()
                });
            }
        );
    }


    render() {

        return (
            <div className="container-fluid">
                <Header></Header>
                <div className="row content">
                    <div className="col-sm-2"></div>
                    <div className="col-sm-8">
                        <div className="form-group row">
                            <div className="col-sm-4">{AuthService.getCurrentUser().username + "'s Profile"}</div>
                            <div className="col-sm-4"></div>
                            <div className="col-sm-4">
                                <div className="row">
                                    <div className="col-sm-6">
                                        <Link to={"/yourRecipe"} > Post Your recipe</Link>
                                    </div>
                                    <div className="col-sm-6">
                                        <Link to={"/changePassword"} > Change Password </Link>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {this.state.content.map(d =>
                            <table className="table table-hover">
                                <tbody>
                                    <tr>
                                        <th scope="row">Full Name</th>
                                        <td colSpan="6">{d.fullName}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Address</th>
                                        <td colSpan="6">{d.address}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Phone Number</th>
                                        <td colSpan="6">{d.phoneNumber}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Email</th>
                                        <td colSpan="6">{d.email}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Gender</th>
                                        <td colSpan="6">{d.gender}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Birthday</th>
                                        <td colSpan="6">{d.birthday}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Avatar</th>
                                        <td colSpan="6">{d.avatar}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Expired Date</th>
                                        <td colSpan="6">{d.expiredDate}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row" colSpan="6" ><a>Click here to paying</a></th>
                                    </tr>
                                    <tr>
                                        <th colSpan="4"></th>
                                        <td>
                                            <Link to={{
                                                pathname: "/updateProfile",
                                                state: {
                                                    stateContent: this.state.content
                                                }
                                            }}>
                                                <Button>
                                                    Upload
                                            </Button>
                                            </Link>
                                        </td>
                                        <td>
                                            <Link to={"/home"} >
                                                <Button>
                                                    Back
                                            </Button>
                                            </Link>
                                        </td>
                                    </tr>
                                </tbody>

                            </table>
                        )}
                    </div>
                    <div className="col-sm-2"></div>
                </div>
            </div>
        )
    }
}
