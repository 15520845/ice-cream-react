import axios from 'axios';
import React, { useEffect, useState } from 'react';
import '../../../App.css';
import authHeader from '../Service/HeadAuth.js'
function UserRecipe() {
    const [stateNewestUserRecipe, setNewestUserRecipe] = useState([])

    useEffect(() => {
        getMostViewRecipe();
    }, [])

    const getMostViewRecipe = () => {
        axios.get("http://localhost:8080/recipe/newestUserRecipe",{ headers: authHeader() }).then(data => {
            setNewestUserRecipe(data.data);
        }).catch(err => alert(err));
    }

    return (

        <div className="scrollable">
            <table className="table table-hover">
                <thead>
                    <tr>
                        <th scope="col" >Newest 10 User's Recipe</th>
                    </tr>
                </thead>

                <tbody>
                    {stateNewestUserRecipe.map(d =>
                        <tr key={d}>
                            <td >{d}</td>
                        </tr>
                    )}
                </tbody>
            </table>
        </div>
    )
}

export default UserRecipe
