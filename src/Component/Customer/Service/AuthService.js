import axios from "axios"
import authHeader from '../Service/HeadAuth.js'

const API_URL = "http://localhost:8080/customer";
class AuthService {
  login(username, password) {
    return axios
      .post(API_URL + "/login", {
        username,
        password
      })
      .then(response => {
        if (response.data.accessToken) {
          localStorage.setItem("user", JSON.stringify(response.data));
        }

        return response.data;
      });
  }

  logout() {
    localStorage.removeItem("user");
  }

  register(username, email, password, fullname, address, phoneNumber, birthday, gender, avatar) {
    return axios.post(API_URL, {
      username,
      email,
      password,
      fullname,
      address,
      phoneNumber,
      birthday,
      gender,
      avatar
    },
      { headers: authHeader() });
  }

  changePassword(password, newPassword, id) {
    return axios.patch(API_URL + "/updatePassword/" + id, {
      password,
      newPassword
    },
      { headers: authHeader() });
  }

  updateProfile(email, fullname, address, phoneNumber, birthday, gender, avatar, id) {
    return axios.patch(API_URL + "/" + id, {
      email,
      fullname,
      address,
      phoneNumber,
      birthday,
      gender,
      avatar
    },
      { headers: authHeader() });
  }

  getCurrentUser() {
    return JSON.parse(localStorage.getItem('user'));;
  }
}

export default new AuthService();