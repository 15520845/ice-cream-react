
import AuthService from "./AuthService"
import axios from "axios"
import authHeader from './HeadAuth'

const API_URL = "http://localhost:8080/customer/";


class CustomerService {

    getProfile(currentUser) {
        return axios.get(API_URL + 'search/' + currentUser, { headers: authHeader() });
    }

    getFaq() {
        return axios.get("http://localhost:8080/faq", { headers: authHeader() });
    }

    getRecipeDetail(id){
        return axios.get("http://localhost:8080/recipe/"+ id );
    }

    getListRecipe(){
        return axios.get("http://localhost:8080/recipe/" , { headers: authHeader() });
    }

    postFeedBack(fullname, title, content){
        return axios.post("http://localhost:8080/feedback",{
            fullname,
            title,
            content
        } ,
        { headers: authHeader() });
    }

    postYourRecip(name, image, description, details, author){
        return axios.post("http://localhost:8080/recipe",{
            name,
            image,
            description,
            details,
            author
        } ,
        { headers: authHeader() });
    }
}

export default new CustomerService();
